﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Web.Http;
using SendGrid;
using System.IO;
using System.Collections.Specialized;
using appOne.Models;
using System.Web;

namespace appOne.Controllers
{
    public class EmailController : ApiController
    {
        [HttpGet]
        public bool SendEmailfromRequest()
        {
            var value = Request.Content.ReadAsStringAsync().Result;
            emailMsg result = JsonConvert.DeserializeObject<emailMsg>(value);

            var emailmsg = new SendGridMessage();
            emailmsg.From = new MailAddress("emailFrom@yourDomain.com", "Display Name of the sender");
            List<String> recipients = new List<String>
            {
                @"addyourRecipients@here.com" //Comma separate to add more forwarding addresses for the email
            };

            emailmsg.AddTo(recipients);
            emailmsg.Subject = result.Subject;
            emailmsg.Text = result.Message;
            emailmsg.EnableFooter("Footer Text Goes Here");
            try
            {
                var transportWeb = new Web("Send grid app key/token goes here");
                transportWeb.DeliverAsync(emailmsg);  
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet]
        public bool _oneEmailSend()
        {
            try
            {
                var value = Request.Content.ReadAsStringAsync().Result; //Read request to extract required parameters
                string imgSrc = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"Path of image file to go in as email body"));
                
                string text = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(@"Path of file to go in as email body"));
                
                var emailmsg = new SendGridMessage();
                emailmsg.From = new MailAddress("email@yourDomain.com", "Display Name for Email");
                emailmsg.AddTo("Recievers email address goes here");
                emailmsg.Subject = "Email Subject line";

                using (MemoryStream m = new MemoryStream(Encoding.UTF8.GetBytes(imgSrc)))
                {
                    emailmsg.EmbedStreamImage(m, "image cid in email body goes here");
                }

                emailmsg.Html= text;
                emailmsg.EnableFooter("\nfooter text goes here");
                emailmsg.EnableOpenTracking(); //Enable or disable email tracking

                var transportWeb = new Web("Send grid app key/token goes here");
                transportWeb.DeliverAsync(emailmsg);   
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
