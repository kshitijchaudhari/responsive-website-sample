﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace appOne.Controllers
{
    public class HomeController : Controller
    {
        string fileVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;
        public HomeController() 
        {
            ViewBag.versionInfo = fileVersion;
        }

        public ActionResult Index()
        {
            ViewBag.pageName = "Index";
            return View();
        }

        public ActionResult Works()
        {
            ViewBag.pageName = "Works";
            return View();
        }

        public ActionResult Resume()
        {
            ViewBag.pageName = "Resume"; 
            return View();
        }

        public ActionResult Skills()
        {
            ViewBag.pageName = "Skills";
            return View();
        }

        public ActionResult Things()
        {
            ViewBag.pageName = "Things";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.pageName = "About";
            return View();
        }

        public ActionResult Legal()
        {
            ViewBag.pageName = "Legal"; 
            return View();
        }
    }
}