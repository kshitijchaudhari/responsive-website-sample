function validateEmail(n) {
    var t = new RegExp("^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,5})+$", "i");
    return t.test(n)
}

function validatePhone(n) {
    if (n == "" || n == "undefined") return !0;
    try {
        var t = new RegExp("^(\\+){0,1}(\\d){10,16}$", "m");
        return t.test(n)
    } catch (i) {
        window.console.log(i)
    }
}

function validateName(n) {
    var t = new RegExp("^[A-Z]{3,15} ?[A-Z]{0,10} ?[A-Z]{2,15}$", "i");
    return t.test(n)
}
jQuery.migrateMute === void 0 && (jQuery.migrateMute = !0),
function(n, t, i) {
    function r(i) {
        var r = t.console;
        o[i] || (o[i] = !0, n.migrateWarnings.push(i), r && r.warn && !n.migrateMute && (r.warn("JQMIGRATE: " + i), n.migrateTrace && r.trace && r.trace()))
    }

    function e(t, u, f, e) {
        if (Object.defineProperty) try {
            return Object.defineProperty(t, u, {
                configurable: !0,
                enumerable: !0,
                get: function() {
                    return r(e), f
                },
                set: function(n) {
                    r(e);
                    f = n
                }
            }), i
        } catch (o) {}
        n._definePropertyBroken = !0;
        t[u] = f
    }
    var o = {}, l, a, v;
    n.migrateWarnings = [];
    !n.migrateMute && t.console && t.console.log && t.console.log("JQMIGRATE: Logging is active");
    n.migrateTrace === i && (n.migrateTrace = !0);
    n.migrateReset = function() {
        o = {};
        n.migrateWarnings.length = 0
    };
    "BackCompat" === document.compatMode && r("jQuery is not compatible with Quirks Mode");
    var s = n("<input/>", {
        size: 1
    }).attr("size") && n.attrFn,
        h = n.attr,
        g = n.attrHooks.value && n.attrHooks.value.get || function() {
            return null
        }, nt = n.attrHooks.value && n.attrHooks.value.set || function() {
            return i
        }, tt = /^(?:input|button)$/i,
        it = /^[238]$/,
        rt = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        ut = /^(?:checked|selected)$/i;
    e(n, "attrFn", s || {}, "jQuery.attrFn is deprecated");
    n.attr = function(t, u, f, e) {
        var o = u.toLowerCase(),
            c = t && t.nodeType;
        return e && (4 > h.length && r("jQuery.fn.attr( props, pass ) is deprecated"), t && !it.test(c) && (s ? u in s : n.isFunction(n.fn[u]))) ? n(t)[u](f) : ("type" === u && f !== i && tt.test(t.nodeName) && t.parentNode && r("Can't change the 'type' of an input or button in IE 6/7/8"), !n.attrHooks[o] && rt.test(o) && (n.attrHooks[o] = {
            get: function(t, r) {
                var u, f = n.prop(t, r);
                return f === !0 || "boolean" != typeof f && (u = t.getAttributeNode(r)) && u.nodeValue !== !1 ? r.toLowerCase() : i
            },
            set: function(t, i, r) {
                var u;
                return i === !1 ? n.removeAttr(t, r) : (u = n.propFix[r] || r, u in t && (t[u] = !0), t.setAttribute(r, r.toLowerCase())), r
            }
        }, ut.test(o) && r("jQuery.fn.attr('" + o + "') may use property instead of attribute")), h.call(n, t, u, f))
    };
    n.attrHooks.value = {
        get: function(n, t) {
            var i = (n.nodeName || "").toLowerCase();
            return "button" === i ? g.apply(this, arguments) : ("input" !== i && "option" !== i && r("jQuery.fn.attr('value') no longer gets properties"), t in n ? n.value : null)
        },
        set: function(n, t) {
            var u = (n.nodeName || "").toLowerCase();
            return "button" === u ? nt.apply(this, arguments) : ("input" !== u && "option" !== u && r("jQuery.fn.attr('value', val) no longer sets properties"), n.value = t, i)
        }
    };
    var f, u, c = n.fn.init,
        ft = n.parseJSON,
        et = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
    n.fn.init = function(t, i, u) {
        var f;
        return t && "string" == typeof t && !n.isPlainObject(i) && (f = et.exec(n.trim(t))) && f[0] && ("<" !== t.charAt(0) && r("$(html) HTML strings must start with '<' character"), f[3] && r("$(html) HTML text after last tag is ignored"), "#" === f[0].charAt(0) && (r("HTML string cannot start with a '#' character"), n.error("JQMIGRATE: Invalid selector string (XSS)")), i && i.context && (i = i.context), n.parseHTML) ? c.call(this, n.parseHTML(f[2], i, !0), i, u) : c.apply(this, arguments)
    };
    n.fn.init.prototype = n.fn;
    n.parseJSON = function(n) {
        return n || null === n ? ft.apply(this, arguments) : (r("jQuery.parseJSON requires a valid JSON string"), null)
    };
    n.uaMatch = function(n) {
        n = n.toLowerCase();
        var t = /(chrome)[ \/]([\w.]+)/.exec(n) || /(webkit)[ \/]([\w.]+)/.exec(n) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(n) || /(msie) ([\w.]+)/.exec(n) || 0 > n.indexOf("compatible") && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(n) || [];
        return {
            browser: t[1] || "",
            version: t[2] || "0"
        }
    };
    n.browser || (f = n.uaMatch(navigator.userAgent), u = {}, f.browser && (u[f.browser] = !0, u.version = f.version), u.chrome ? u.webkit = !0 : u.webkit && (u.safari = !0), n.browser = u);
    e(n, "browser", n.browser, "jQuery.browser is deprecated");
    n.sub = function() {
        function t(n, i) {
            return new t.fn.init(n, i)
        }
        n.extend(!0, t, this);
        t.superclass = this;
        t.fn = t.prototype = this();
        t.fn.constructor = t;
        t.sub = this.sub;
        t.fn.init = function(r, u) {
            return u && u instanceof n && !(u instanceof t) && (u = t(u)), n.fn.init.call(this, r, u, i)
        };
        t.fn.init.prototype = t.fn;
        var i = t(document);
        return r("jQuery.sub() is deprecated"), t
    };
    n.ajaxSetup({
        converters: {
            "text json": n.parseJSON
        }
    });
    l = n.fn.data;
    n.fn.data = function(t) {
        var f, u, e = this[0];
        return !e || "events" !== t || 1 !== arguments.length || (f = n.data(e, t), u = n._data(e, t), f !== i && f !== u || u === i) ? l.apply(this, arguments) : (r("Use of jQuery.fn.data('events') is deprecated"), u)
    };
    a = /\/(java|ecma)script/i;
    v = n.fn.andSelf || n.fn.addBack;
    n.fn.andSelf = function() {
        return r("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"), v.apply(this, arguments)
    };
    n.clean || (n.clean = function(t, u, f, e) {
        u = u || document;
        u = !u.nodeType && u[0] || u;
        u = u.ownerDocument || u;
        r("jQuery.clean() is deprecated");
        var s, o, c, l, h = [];
        if (n.merge(h, n.buildFragment(t, u).childNodes), f)
            for (c = function(n) {
                return !n.type || a.test(n.type) ? e ? e.push(n.parentNode ? n.parentNode.removeChild(n) : n) : f.appendChild(n) : i
            }, s = 0; null != (o = h[s]); s++) n.nodeName(o, "script") && c(o) || (f.appendChild(o), o.getElementsByTagName !== i && (l = n.grep(n.merge([], o.getElementsByTagName("script")), c), h.splice.apply(h, [s + 1, 0].concat(l)), s += l.length));
        return h
    });
    var ot = n.event.add,
        st = n.event.remove,
        ht = n.event.trigger,
        ct = n.fn.toggle,
        y = n.fn.live,
        p = n.fn.die,
        w = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
        b = RegExp("\\b(?:" + w + ")\\b"),
        k = /(?:^|\s)hover(\.\S+|)\b/,
        d = function(t) {
            return "string" != typeof t || n.event.special.hover ? t : (k.test(t) && r("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"), t && t.replace(k, "mouseenter$1 mouseleave$1"))
        };
    n.event.props && "attrChange" !== n.event.props[0] && n.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement");
    n.event.dispatch && e(n.event, "handle", n.event.dispatch, "jQuery.event.handle is undocumented and deprecated");
    n.event.add = function(n, t, i, u, f) {
        n !== document && b.test(t) && r("AJAX events should be attached to document: " + t);
        ot.call(this, n, d(t || ""), i, u, f)
    };
    n.event.remove = function(n, t, i, r, u) {
        st.call(this, n, d(t) || "", i, r, u)
    };
    n.fn.error = function() {
        var n = Array.prototype.slice.call(arguments, 0);
        return r("jQuery.fn.error() is deprecated"), n.splice(0, 0, "error"), arguments.length ? this.bind.apply(this, n) : (this.triggerHandler.apply(this, n), this)
    };
    n.fn.toggle = function(t, i) {
        if (!n.isFunction(t) || !n.isFunction(i)) return ct.apply(this, arguments);
        r("jQuery.fn.toggle(handler, handler...) is deprecated");
        var u = arguments,
            e = t.guid || n.guid++,
            f = 0,
            o = function(i) {
                var r = (n._data(this, "lastToggle" + t.guid) || 0) % f;
                return n._data(this, "lastToggle" + t.guid, r + 1), i.preventDefault(), u[r].apply(this, arguments) || !1
            };
        for (o.guid = e; u.length > f;) u[f++].guid = e;
        return this.click(o)
    };
    n.fn.live = function(t, i, u) {
        return r("jQuery.fn.live() is deprecated"), y ? y.apply(this, arguments) : (n(this.context).on(t, this.selector, i, u), this)
    };
    n.fn.die = function(t, i) {
        return r("jQuery.fn.die() is deprecated"), p ? p.apply(this, arguments) : (n(this.context).off(t, this.selector || "**", i), this)
    };
    n.event.trigger = function(n, t, i, u) {
        return i || b.test(n) || r("Global events are undocumented and deprecated"), ht.call(this, n, t, i || document, u)
    };
    n.each(w.split("|"), function(t, i) {
        n.event.special[i] = {
            setup: function() {
                var t = this;
                return t !== document && (n.event.add(document, i + "." + n.guid, function() {
                    n.event.trigger(i, null, t, !0)
                }), n._data(this, i, n.guid++)), !1
            },
            teardown: function() {
                return this !== document && n.event.remove(document, i + "." + n._data(this, i)), !1
            }
        }
    })
}(jQuery, window);
! function(n) {
    "function" == typeof define && define.amd ? define(["jquery"], n) : n(jQuery)
}(function(n) {
    n.extend(n.fn, {
        validate: function(t) {
            if (!this.length) return void(t && t.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));
            var i = n.data(this[0], "validator");
            return i ? i : (this.attr("novalidate", "novalidate"), i = new n.validator(t, this[0]), n.data(this[0], "validator", i), i.settings.onsubmit && (this.validateDelegate(":submit", "click", function(t) {
                i.settings.submitHandler && (i.submitButton = t.target);
                n(t.target).hasClass("cancel") && (i.cancelSubmit = !0);
                void 0 !== n(t.target).attr("formnovalidate") && (i.cancelSubmit = !0)
            }), this.submit(function(t) {
                function r() {
                    var u, r;
                    return i.settings.submitHandler ? (i.submitButton && (u = n("<input type='hidden'/>").attr("name", i.submitButton.name).val(n(i.submitButton).val()).appendTo(i.currentForm)), r = i.settings.submitHandler.call(i, i.currentForm, t), i.submitButton && u.remove(), void 0 !== r ? r : !1) : !0
                }
                return i.settings.debug && t.preventDefault(), i.cancelSubmit ? (i.cancelSubmit = !1, r()) : i.form() ? i.pendingRequest ? (i.formSubmitted = !0, !1) : r() : (i.focusInvalid(), !1)
            })), i)
        },
        valid: function() {
            var t, i;
            return n(this[0]).is("form") ? t = this.validate().form() : (t = !0, i = n(this[0].form).validate(), this.each(function() {
                t = i.element(this) && t
            })), t
        },
        removeAttrs: function(t) {
            var i = {}, r = this;
            return n.each(t.split(/\s/), function(n, t) {
                i[t] = r.attr(t);
                r.removeAttr(t)
            }), i
        },
        rules: function(t, i) {
            var e, s, f, u, o, h, r = this[0];
            if (t) switch (e = n.data(r.form, "validator").settings, s = e.rules, f = n.validator.staticRules(r), t) {
                case "add":
                    n.extend(f, n.validator.normalizeRule(i));
                    delete f.messages;
                    s[r.name] = f;
                    i.messages && (e.messages[r.name] = n.extend(e.messages[r.name], i.messages));
                    break;
                case "remove":
                    return i ? (h = {}, n.each(i.split(/\s/), function(t, i) {
                        h[i] = f[i];
                        delete f[i];
                        "required" === i && n(r).removeAttr("aria-required")
                    }), h) : (delete s[r.name], f)
            }
            return u = n.validator.normalizeRules(n.extend({}, n.validator.classRules(r), n.validator.attributeRules(r), n.validator.dataRules(r), n.validator.staticRules(r)), r), u.required && (o = u.required, delete u.required, u = n.extend({
                required: o
            }, u), n(r).attr("aria-required", "true")), u.remote && (o = u.remote, delete u.remote, u = n.extend(u, {
                remote: o
            })), u
        }
    });
    n.extend(n.expr[":"], {
        blank: function(t) {
            return !n.trim("" + n(t).val())
        },
        filled: function(t) {
            return !!n.trim("" + n(t).val())
        },
        unchecked: function(t) {
            return !n(t).prop("checked")
        }
    });
    n.validator = function(t, i) {
        this.settings = n.extend(!0, {}, n.validator.defaults, t);
        this.currentForm = i;
        this.init()
    };
    n.validator.format = function(t, i) {
        return 1 === arguments.length ? function() {
            var i = n.makeArray(arguments);
            return i.unshift(t), n.validator.format.apply(this, i)
        } : (arguments.length > 2 && i.constructor !== Array && (i = n.makeArray(arguments).slice(1)), i.constructor !== Array && (i = [i]), n.each(i, function(n, i) {
            t = t.replace(new RegExp("\\{" + n + "\\}", "g"), function() {
                return i
            })
        }), t)
    };
    n.extend(n.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusCleanup: !1,
            focusInvalid: !0,
            errorContainer: n([]),
            errorLabelContainer: n([]),
            onsubmit: !0,
            ignore: ":hidden",
            ignoreTitle: !1,
            onfocusin: function(n) {
                this.lastActive = n;
                this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, n, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(n)))
            },
            onfocusout: function(n) {
                !this.checkable(n) && (n.name in this.submitted || !this.optional(n)) && this.element(n)
            },
            onkeyup: function(n, t) {
                (9 !== t.which || "" !== this.elementValue(n)) && (n.name in this.submitted || n === this.lastElement) && this.element(n)
            },
            onclick: function(n) {
                n.name in this.submitted ? this.element(n) : n.parentNode.name in this.submitted && this.element(n.parentNode)
            },
            highlight: function(t, i, r) {
                "radio" === t.type ? this.findByName(t.name).addClass(i).removeClass(r) : n(t).addClass(i).removeClass(r)
            },
            unhighlight: function(t, i, r) {
                "radio" === t.type ? this.findByName(t.name).removeClass(i).addClass(r) : n(t).removeClass(i).addClass(r)
            }
        },
        setDefaults: function(t) {
            n.extend(n.validator.defaults, t)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date ( ISO ).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            maxlength: n.validator.format("Please enter no more than {0} characters."),
            minlength: n.validator.format("Please enter at least {0} characters."),
            rangelength: n.validator.format("Please enter a value between {0} and {1} characters long."),
            range: n.validator.format("Please enter a value between {0} and {1}."),
            max: n.validator.format("Please enter a value less than or equal to {0}."),
            min: n.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: !1,
        prototype: {
            init: function() {
                function i(t) {
                    var r = n.data(this[0].form, "validator"),
                        u = "on" + t.type.replace(/^validate/, ""),
                        i = r.settings;
                    i[u] && !this.is(i.ignore) && i[u].call(r, this[0], t)
                }
                this.labelContainer = n(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || n(this.currentForm);
                this.containers = n(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();
                var t, r = this.groups = {};
                n.each(this.settings.groups, function(t, i) {
                    "string" == typeof i && (i = i.split(/\s/));
                    n.each(i, function(n, i) {
                        r[i] = t
                    })
                });
                t = this.settings.rules;
                n.each(t, function(i, r) {
                    t[i] = n.validator.normalizeRule(r)
                });
                n(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']", "focusin focusout keyup", i).validateDelegate("select, option, [type='radio'], [type='checkbox']", "click", i);
                this.settings.invalidHandler && n(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler);
                n(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true")
            },
            form: function() {
                return this.checkForm(), n.extend(this.submitted, this.errorMap), this.invalid = n.extend({}, this.errorMap), this.valid() || n(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid()
            },
            checkForm: function() {
                this.prepareForm();
                for (var n = 0, t = this.currentElements = this.elements(); t[n]; n++) this.check(t[n]);
                return this.valid()
            },
            element: function(t) {
                var u = this.clean(t),
                    i = this.validationTargetFor(u),
                    r = !0;
                return this.lastElement = i, void 0 === i ? delete this.invalid[u.name] : (this.prepareElement(i), this.currentElements = n(i), r = this.check(i) !== !1, r ? delete this.invalid[i.name] : this.invalid[i.name] = !0), n(t).attr("aria-invalid", !r), this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), r
            },
            showErrors: function(t) {
                if (t) {
                    n.extend(this.errorMap, t);
                    this.errorList = [];
                    for (var i in t) this.errorList.push({
                        message: t[i],
                        element: this.findByName(i)[0]
                    });
                    this.successList = n.grep(this.successList, function(n) {
                        return !(n.name in t)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            },
            resetForm: function() {
                n.fn.resetForm && n(this.currentForm).resetForm();
                this.submitted = {};
                this.lastElement = null;
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass).removeData("previousValue").removeAttr("aria-invalid")
            },
            numberOfInvalids: function() {
                return this.objectLength(this.invalid)
            },
            objectLength: function(n) {
                var i, t = 0;
                for (i in n) t++;
                return t
            },
            hideErrors: function() {
                this.hideThese(this.toHide)
            },
            hideThese: function(n) {
                n.not(this.containers).text("");
                this.addWrapper(n).hide()
            },
            valid: function() {
                return 0 === this.size()
            },
            size: function() {
                return this.errorList.length
            },
            focusInvalid: function() {
                if (this.settings.focusInvalid) try {
                    n(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                } catch (t) {}
            },
            findLastActive: function() {
                var t = this.lastActive;
                return t && 1 === n.grep(this.errorList, function(n) {
                    return n.element.name === t.name
                }).length && t
            },
            elements: function() {
                var t = this,
                    i = {};
                return n(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled], [readonly]").not(this.settings.ignore).filter(function() {
                    return !this.name && t.settings.debug && window.console && console.error("%o has no name assigned", this), this.name in i || !t.objectLength(n(this).rules()) ? !1 : (i[this.name] = !0, !0)
                })
            },
            clean: function(t) {
                return n(t)[0]
            },
            errors: function() {
                var t = this.settings.errorClass.split(" ").join(".");
                return n(this.settings.errorElement + "." + t, this.errorContext)
            },
            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = n([]);
                this.toHide = n([]);
                this.currentElements = n([])
            },
            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers)
            },
            prepareElement: function(n) {
                this.reset();
                this.toHide = this.errorsFor(n)
            },
            elementValue: function(t) {
                var i, u = n(t),
                    r = t.type;
                return "radio" === r || "checkbox" === r ? n("input[name='" + t.name + "']:checked").val() : "number" === r && "undefined" != typeof t.validity ? t.validity.badInput ? !1 : u.val() : (i = u.val(), "string" == typeof i ? i.replace(/\r/g, "") : i)
            },
            check: function(t) {
                t = this.validationTargetFor(this.clean(t));
                var i, r, u, f = n(t).rules(),
                    s = n.map(f, function(n, t) {
                        return t
                    }).length,
                    e = !1,
                    h = this.elementValue(t);
                for (r in f) {
                    u = {
                        method: r,
                        parameters: f[r]
                    };
                    try {
                        if (i = n.validator.methods[r].call(this, h, t, u.parameters), "dependency-mismatch" === i && 1 === s) {
                            e = !0;
                            continue
                        }
                        if (e = !1, "pending" === i) return void(this.toHide = this.toHide.not(this.errorsFor(t)));
                        if (!i) return this.formatAndAdd(t, u), !1
                    } catch (o) {
                        throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + t.id + ", check the '" + u.method + "' method.", o), o;
                    }
                }
                if (!e) return this.objectLength(f) && this.successList.push(t), !0
            },
            customDataMessage: function(t, i) {
                return n(t).data("msg" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()) || n(t).data("msg")
            },
            customMessage: function(n, t) {
                var i = this.settings.messages[n];
                return i && (i.constructor === String ? i : i[t])
            },
            findDefined: function() {
                for (var n = 0; n < arguments.length; n++)
                    if (void 0 !== arguments[n]) return arguments[n];
                return void 0
            },
            defaultMessage: function(t, i) {
                return this.findDefined(this.customMessage(t.name, i), this.customDataMessage(t, i), !this.settings.ignoreTitle && t.title || void 0, n.validator.messages[i], "<strong>Warning: No message defined for " + t.name + "<\/strong>")
            },
            formatAndAdd: function(t, i) {
                var r = this.defaultMessage(t, i.method),
                    u = /\$?\{(\d+)\}/g;
                "function" == typeof r ? r = r.call(this, i.parameters, t) : u.test(r) && (r = n.validator.format(r.replace(u, "{$1}"), i.parameters));
                this.errorList.push({
                    message: r,
                    element: t,
                    method: i.method
                });
                this.errorMap[t.name] = r;
                this.submitted[t.name] = r
            },
            addWrapper: function(n) {
                return this.settings.wrapper && (n = n.add(n.parent(this.settings.wrapper))), n
            },
            defaultShowErrors: function() {
                for (var i, t, n = 0; this.errorList[n]; n++) t = this.errorList[n], this.settings.highlight && this.settings.highlight.call(this, t.element, this.settings.errorClass, this.settings.validClass), this.showLabel(t.element, t.message);
                if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success)
                    for (n = 0; this.successList[n]; n++) this.showLabel(this.successList[n]);
                if (this.settings.unhighlight)
                    for (n = 0, i = this.validElements(); i[n]; n++) this.settings.unhighlight.call(this, i[n], this.settings.errorClass, this.settings.validClass);
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show()
            },
            validElements: function() {
                return this.currentElements.not(this.invalidElements())
            },
            invalidElements: function() {
                return n(this.errorList).map(function() {
                    return this.element
                })
            },
            showLabel: function(t, i) {
                var u, o, e, r = this.errorsFor(t),
                    s = this.idOrName(t),
                    f = n(t).attr("aria-describedby");
                r.length ? (r.removeClass(this.settings.validClass).addClass(this.settings.errorClass), r.html(i)) : (r = n("<" + this.settings.errorElement + ">").attr("id", s + "-error").addClass(this.settings.errorClass).html(i || ""), u = r, this.settings.wrapper && (u = r.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(u) : this.settings.errorPlacement ? this.settings.errorPlacement(u, n(t)) : u.insertAfter(t), r.is("label") ? r.attr("for", s) : 0 === r.parents("label[for='" + s + "']").length && (e = r.attr("id").replace(/(:|\.|\[|\])/g, "\\$1"), f ? f.match(new RegExp("\\b" + e + "\\b")) || (f += " " + e) : f = e, n(t).attr("aria-describedby", f), o = this.groups[t.name], o && n.each(this.groups, function(t, i) {
                    i === o && n("[name='" + t + "']", this.currentForm).attr("aria-describedby", r.attr("id"))
                })));
                !i && this.settings.success && (r.text(""), "string" == typeof this.settings.success ? r.addClass(this.settings.success) : this.settings.success(r, t));
                this.toShow = this.toShow.add(r)
            },
            errorsFor: function(t) {
                var r = this.idOrName(t),
                    u = n(t).attr("aria-describedby"),
                    i = "label[for='" + r + "'], label[for='" + r + "'] *";
                return u && (i = i + ", #" + u.replace(/\s+/g, ", #")), this.errors().filter(i)
            },
            idOrName: function(n) {
                return this.groups[n.name] || (this.checkable(n) ? n.name : n.id || n.name)
            },
            validationTargetFor: function(t) {
                return this.checkable(t) && (t = this.findByName(t.name)), n(t).not(this.settings.ignore)[0]
            },
            checkable: function(n) {
                return /radio|checkbox/i.test(n.type)
            },
            findByName: function(t) {
                return n(this.currentForm).find("[name='" + t + "']")
            },
            getLength: function(t, i) {
                switch (i.nodeName.toLowerCase()) {
                    case "select":
                        return n("option:selected", i).length;
                    case "input":
                        if (this.checkable(i)) return this.findByName(i.name).filter(":checked").length
                }
                return t.length
            },
            depend: function(n, t) {
                return this.dependTypes[typeof n] ? this.dependTypes[typeof n](n, t) : !0
            },
            dependTypes: {
                boolean: function(n) {
                    return n
                },
                string: function(t, i) {
                    return !!n(t, i.form).length
                },
                "function": function(n, t) {
                    return n(t)
                }
            },
            optional: function(t) {
                var i = this.elementValue(t);
                return !n.validator.methods.required.call(this, i, t) && "dependency-mismatch"
            },
            startRequest: function(n) {
                this.pending[n.name] || (this.pendingRequest++, this.pending[n.name] = !0)
            },
            stopRequest: function(t, i) {
                this.pendingRequest--;
                this.pendingRequest < 0 && (this.pendingRequest = 0);
                delete this.pending[t.name];
                i && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (n(this.currentForm).submit(), this.formSubmitted = !1) : !i && 0 === this.pendingRequest && this.formSubmitted && (n(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1)
            },
            previousValue: function(t) {
                return n.data(t, "previousValue") || n.data(t, "previousValue", {
                    old: null,
                    valid: !0,
                    message: this.defaultMessage(t, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {
                required: !0
            },
            email: {
                email: !0
            },
            url: {
                url: !0
            },
            date: {
                date: !0
            },
            dateISO: {
                dateISO: !0
            },
            number: {
                number: !0
            },
            digits: {
                digits: !0
            },
            creditcard: {
                creditcard: !0
            }
        },
        addClassRules: function(t, i) {
            t.constructor === String ? this.classRuleSettings[t] = i : n.extend(this.classRuleSettings, t)
        },
        classRules: function(t) {
            var i = {}, r = n(t).attr("class");
            return r && n.each(r.split(" "), function() {
                this in n.validator.classRuleSettings && n.extend(i, n.validator.classRuleSettings[this])
            }), i
        },
        attributeRules: function(t) {
            var r, i, u = {}, e = n(t),
                f = t.getAttribute("type");
            for (r in n.validator.methods) "required" === r ? (i = t.getAttribute(r), "" === i && (i = !0), i = !! i) : i = e.attr(r), /min|max/.test(r) && (null === f || /number|range|text/.test(f)) && (i = Number(i)), i || 0 === i ? u[r] = i : f === r && "range" !== f && (u[r] = !0);
            return u.maxlength && /-1|2147483647|524288/.test(u.maxlength) && delete u.maxlength, u
        },
        dataRules: function(t) {
            var i, r, u = {}, f = n(t);
            for (i in n.validator.methods) r = f.data("rule" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()), void 0 !== r && (u[i] = r);
            return u
        },
        staticRules: function(t) {
            var i = {}, r = n.data(t.form, "validator");
            return r.settings.rules && (i = n.validator.normalizeRule(r.settings.rules[t.name]) || {}), i
        },
        normalizeRules: function(t, i) {
            return n.each(t, function(r, u) {
                if (u === !1) return void delete t[r];
                if (u.param || u.depends) {
                    var f = !0;
                    switch (typeof u.depends) {
                        case "string":
                            f = !! n(u.depends, i.form).length;
                            break;
                        case "function":
                            f = u.depends.call(i, i)
                    }
                    f ? t[r] = void 0 !== u.param ? u.param : !0 : delete t[r]
                }
            }), n.each(t, function(r, u) {
                t[r] = n.isFunction(u) ? u(i) : u
            }), n.each(["minlength", "maxlength"], function() {
                t[this] && (t[this] = Number(t[this]))
            }), n.each(["rangelength", "range"], function() {
                var i;
                t[this] && (n.isArray(t[this]) ? t[this] = [Number(t[this][0]), Number(t[this][1])] : "string" == typeof t[this] && (i = t[this].replace(/[\[\]]/g, "").split(/[\s,]+/), t[this] = [Number(i[0]), Number(i[1])]))
            }), n.validator.autoCreateRanges && (null != t.min && null != t.max && (t.range = [t.min, t.max], delete t.min, delete t.max), null != t.minlength && null != t.maxlength && (t.rangelength = [t.minlength, t.maxlength], delete t.minlength, delete t.maxlength)), t
        },
        normalizeRule: function(t) {
            if ("string" == typeof t) {
                var i = {};
                n.each(t.split(/\s/), function() {
                    i[this] = !0
                });
                t = i
            }
            return t
        },
        addMethod: function(t, i, r) {
            n.validator.methods[t] = i;
            n.validator.messages[t] = void 0 !== r ? r : n.validator.messages[t];
            i.length < 3 && n.validator.addClassRules(t, n.validator.normalizeRule(t))
        },
        methods: {
            required: function(t, i, r) {
                if (!this.depend(r, i)) return "dependency-mismatch";
                if ("select" === i.nodeName.toLowerCase()) {
                    var u = n(i).val();
                    return u && u.length > 0
                }
                return this.checkable(i) ? this.getLength(t, i) > 0 : n.trim(t).length > 0
            },
            email: function(n, t) {
                return this.optional(t) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(n)
            },
            url: function(n, t) {
                return this.optional(t) || /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(n)
            },
            date: function(n, t) {
                return this.optional(t) || !/Invalid|NaN/.test(new Date(n).toString())
            },
            dateISO: function(n, t) {
                return this.optional(t) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(n)
            },
            number: function(n, t) {
                return this.optional(t) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(n)
            },
            digits: function(n, t) {
                return this.optional(t) || /^\d+$/.test(n)
            },
            creditcard: function(n, t) {
                if (this.optional(t)) return "dependency-mismatch";
                if (/[^0-9 \-]+/.test(n)) return !1;
                var i, f, e = 0,
                    r = 0,
                    u = !1;
                if (n = n.replace(/\D/g, ""), n.length < 13 || n.length > 19) return !1;
                for (i = n.length - 1; i >= 0; i--) f = n.charAt(i), r = parseInt(f, 10), u && (r *= 2) > 9 && (r -= 9), e += r, u = !u;
                return e % 10 == 0
            },
            minlength: function(t, i, r) {
                var u = n.isArray(t) ? t.length : this.getLength(t, i);
                return this.optional(i) || u >= r
            },
            maxlength: function(t, i, r) {
                var u = n.isArray(t) ? t.length : this.getLength(t, i);
                return this.optional(i) || r >= u
            },
            rangelength: function(t, i, r) {
                var u = n.isArray(t) ? t.length : this.getLength(t, i);
                return this.optional(i) || u >= r[0] && u <= r[1]
            },
            min: function(n, t, i) {
                return this.optional(t) || n >= i
            },
            max: function(n, t, i) {
                return this.optional(t) || i >= n
            },
            range: function(n, t, i) {
                return this.optional(t) || n >= i[0] && n <= i[1]
            },
            equalTo: function(t, i, r) {
                var u = n(r);
                return this.settings.onfocusout && u.unbind(".validate-equalTo").bind("blur.validate-equalTo", function() {
                    n(i).valid()
                }), t === u.val()
            },
            remote: function(t, i, r) {
                if (this.optional(i)) return "dependency-mismatch";
                var u, e, f = this.previousValue(i);
                return this.settings.messages[i.name] || (this.settings.messages[i.name] = {}), f.originalMessage = this.settings.messages[i.name].remote, this.settings.messages[i.name].remote = f.message, r = "string" == typeof r && {
                    url: r
                } || r, f.old === t ? f.valid : (f.old = t, u = this, this.startRequest(i), e = {}, e[i.name] = t, n.ajax(n.extend(!0, {
                    url: r,
                    mode: "abort",
                    port: "validate" + i.name,
                    dataType: "json",
                    data: e,
                    context: u.currentForm,
                    success: function(r) {
                        var o, e, h, s = r === !0 || "true" === r;
                        u.settings.messages[i.name].remote = f.originalMessage;
                        s ? (h = u.formSubmitted, u.prepareElement(i), u.formSubmitted = h, u.successList.push(i), delete u.invalid[i.name], u.showErrors()) : (o = {}, e = r || u.defaultMessage(i, "remote"), o[i.name] = f.message = n.isFunction(e) ? e(t) : e, u.invalid[i.name] = !0, u.showErrors(o));
                        f.valid = s;
                        u.stopRequest(i, s)
                    }
                }, r)), "pending")
            }
        }
    });
    n.format = function() {
        throw "$.format has been deprecated. Please use $.validator.format instead.";
    };
    var i, t = {};
    n.ajaxPrefilter ? n.ajaxPrefilter(function(n, i, r) {
        var u = n.port;
        "abort" === n.mode && (t[u] && t[u].abort(), t[u] = r)
    }) : (i = n.ajax, n.ajax = function(r) {
        var f = ("mode" in r ? r : n.ajaxSettings).mode,
            u = ("port" in r ? r : n.ajaxSettings).port;
        return "abort" === f ? (t[u] && t[u].abort(), t[u] = i.apply(this, arguments), t[u]) : i.apply(this, arguments)
    });
    n.extend(n.fn, {
        validateDelegate: function(t, i, r) {
            return this.bind(i, function(i) {
                var u = n(i.target);
                if (u.is(t)) return r.apply(u, arguments)
            })
        }
    })
});
jQuery(document).ready(function(n) {
    function t(t) {
        n(t).css({
            height: n(window).height()
        })
    }

    function i(i) {
        t(i);
        n(i).css({
            width: n(window).width()
        })
    }
    localStorage.getItem("scrollToContact") == 1 && (localStorage.removeItem("scrollToContact"), window.location.hash = "contact");
    switch (pageName) {
        case "Index":
            n("#nav li").removeClass("current");
            n("#homeLink").parent().addClass("current");
            break;
        case "Works":
            n("#nav li").removeClass("current");
            n("#worksLink").parent().addClass("current");
            break;
        case "Resume":
            n("#nav li").removeClass("current");
            n("#resumeLink").parent().addClass("current");
            break;
        case "Skills":
            n("#nav li").removeClass("current");
            n("#skillsLink").parent().addClass("current");
            break;
        case "Things":
            n("#nav li").removeClass("current");
            n("#thingsLink").parent().addClass("current");
            break;
        case "About":
            n("#nav li").removeClass("current");
            n("#aboutLink").parent().addClass("current")
    }
    n("#homeLink").click(function(t) {
        pageName != "Index" && (t.preventDefault(), n(location).attr("pathname", "/Home/Index"))
    });
    n("#contactFormClick").click(function(t) {
        pageName != "Index" && (t.preventDefault(), n(location).attr("pathname", "/Home/Index"), localStorage.setItem("scrollToContact", 1))
    });
    n("a[href*=#]:not([href=#]):not(.item-wrap a):not(#getQR):not(#nav-wrap a)").click(function(t) {
        if (t.stopImmediatePropagation(), t.preventDefault(), location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var i = n(this.hash);
            if (i = i.length ? i : n("[name=" + this.hash.slice(1) + "]"), i.length) return n("html,body").animate({
                scrollTop: i.offset().top
            }, 1e3), !1
        }
    }),
    function(n) {
        var t = n(window).scrollTop() + 200;
        n(function() {
            (function(n) {
                n.fn.slideToPos = function() {
                    var t, i;
                    if (typeof n(this).position() != "undefined" || n(this).position() != null) return t = n(this).position().left + n(this).width() / 2 - n(".menu a.knob").width() / 2 - 2, n(".menu a.knob").css("left", t), i = n(this).parent().parent().parent().children(".bar").first().children(".before").first(), i.css("width", t + 20).css("backgroundColor", n(n(this).attr("href")).css("backgroundColor")), this
                }
            })(jQuery);
            n(".menu ul li.active a").slideToPos();
            n(".menu ul li a").click(function(t) {
                t.preventDefault();
                n(this).slideToPos();
                n("html, body").animate({
                    scrollTop: n(this.hash).offset().top
                }, 400)
            });
            n(window).scroll(function() {
                var t = n(window).scrollTop();
                n(".Scontainer").each(function() {
                    var r = n(this).offset().top,
                        u = r + n(this).height(),
                        f = n(this).attr("id"),
                        i = n('a[href$="' + f + '"]').parent();
                    if (t > r && t < u) {
                        if (i.hasClass("active")) return !1;
                        i.siblings().andSelf().removeClass("active");
                        i.addClass("active");
                        n(".menu ul li.active a").slideToPos()
                    }
                })
            })
        })
    }(jQuery);
    n(".item-wrap a").magnificPopup({
        type: "inline",
        fixedContentPos: !1,
        removalDelay: 200,
        showCloseBtn: !1,
        mainClass: "mfp-fade"
    });
    n(document).on("click", ".popup-modal-dismiss", function(t) {
        t.preventDefault();
        n.magnificPopup.close()
    });
    n("#getQR").magnificPopup({
        type: "inline",
        fixedContentPos: !1,
        removalDelay: 200,
        showCloseBtn: !0,
        mainClass: "mfp-fade",
        midClick: !0,
        modal: !1,
        closeMarkup: '<button class="mfp-close whiteColor">&times;<\/button>'
    });
    setTimeout(function() {
        n("h1.responsive-headline").fitText(1, {
            minFontSize: "40px",
            maxFontSize: "90px"
        })
    }, 500);
    n("header").css({
        height: n(window).height()
    });
    n(window).on("resize", function() {
        n("header").css({
            height: n(window).height()
        });
        n("body").css({
            width: n(window).width()
        })
    });
    t("#landingResume");
    i("#landingResume");
    n(window).on("scroll", function() {
        var t = n("header").height(),
            i = n(window).scrollTop(),
            r = n("#nav-wrap");
        i > t * .2 && i < t && n(window).outerWidth() > 768 ? r.fadeOut("fast") : i < t * .2 ? r.removeClass("opaque").fadeIn("fast") : r.addClass("opaque").fadeIn("fast")
    });
    n("form#contactForm button.submit").click(function() {
        var t = n("#contactForm #contactName").val(),
            i = n("#contactForm #contactEmail").val(),
            u = n("#contactForm #contactSubject").val(),
            f = n("#contactForm #contactMessage").val(),
            r;
        if (validateName(t) && validateEmail(i)) return r = '{ "contactName":"' + t + '","contactEmail":"' + i + '","contactSubject":"' + u + '","contactMessage":"' + f + '" }', n("#image-loader").fadeIn(), n.ajax({
            type: "POST",
            url: "/Api/Email/Send",
            data: r,
            contentType: "application/json",
            success: function(t) {
                t ? (n("#image-loader").fadeOut(), n("#message-warning").hide(), n("#contactForm").fadeOut(), n("#message-success").fadeIn()) : (n("#image-loader").fadeOut(), n("#message-warning").html(t), n("#message-warning").fadeIn())
            }
        }), !1;
        n("#contactErrorMsg").removeClass("mfp-hide")
    });
    n("#go-top").slideUp();
    n("#go-top").click(function(t) {
        t.preventDefault();
        n("html, body").stop().animate({
            scrollTop: "0px"
        }, 800)
    });
    n(window).scroll(function() {
        n(this).scrollTop() != 0 ? n("#go-top").slideDown() : n("#go-top").slideUp()
    });
    n(document).ready(function() {
        var t = n(".colorDot");
        for (var i in t)(typeof t[i].innerHTML != "undefined" || t[i].innerHTML != null) && (t[i].innerHTML = t[i].innerHTML.replace(/([ij])/g, '<span class="$1">$1<\/span>'))
    });
    n("#titleLogo").click(function(t) {
        t.preventDefault();
        var i = n("#nameWebsite").text();
        n("#nameWebsite").text(n("#nameWebsite").attr("title"));
        n("#nameWebsite").attr("title", i)
    })
}),
function(n) {
    n.fn.fitText = function(t, i) {
        var u = t || 1,
            r = n.extend({
                minFontSize: Number.NEGATIVE_INFINITY,
                maxFontSize: Number.POSITIVE_INFINITY
            }, i);
        return this.each(function() {
            var t = n(this),
                i = function() {
                    t.css("font-size", Math.max(Math.min(t.width() / (u * 10), parseFloat(r.maxFontSize)), parseFloat(r.minFontSize)))
                };
            i();
            n(window).on("resize.fittext orientationchange.fittext", i)
        })
    }
}(jQuery),
function(n) {
    var a = "Close",
        et = "BeforeClose",
        vt = "AfterClose",
        yt = "BeforeAppend",
        ot = "MarkupParse",
        st = "Open",
        pt = "Change",
        d = "mfp",
        r = "." + d,
        g = "mfp-ready",
        ht = "mfp-removing",
        wt = "mfp-prevent-close",
        t, v = function() {}, nt = !! window.jQuery,
        tt, f = n(window),
        l, o, y, s, ct, u = function(n, i) {
            t.ev.on(d + n + r, i)
        }, h = function(t, i, r, u) {
            var f = document.createElement("div");
            return f.className = "mfp-" + t, r && (f.innerHTML = r), u ? i && i.appendChild(f) : (f = n(f), i && f.appendTo(i)), f
        }, i = function(i, r) {
            t.ev.triggerHandler(d + i, r);
            t.st.callbacks && (i = i.charAt(0).toLowerCase() + i.slice(1), t.st.callbacks[i] && t.st.callbacks[i].apply(t, n.isArray(r) ? r : [r]))
        }, it = function(i) {
            return i === ct && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = n(t.st.closeMarkup.replace("%title%", t.st.tClose)), ct = i), t.currTemplate.closeBtn
        }, rt = function() {
            n.magnificPopup.instance || (t = new v, t.init(), n.magnificPopup.instance = t)
        }, bt = function() {
            var n = document.createElement("p").style,
                t = ["ms", "O", "Moz", "Webkit"];
            if (n.transition !== undefined) return !0;
            while (t.length)
                if (t.pop() + "Transition" in n) return !0;
            return !1
        }, p, c, w, b, ut, e, lt, ft, at, k;
    v.prototype = {
        constructor: v,
        init: function() {
            var i = navigator.appVersion;
            t.isIE7 = i.indexOf("MSIE 7.") !== -1;
            t.isIE8 = i.indexOf("MSIE 8.") !== -1;
            t.isLowIE = t.isIE7 || t.isIE8;
            t.isAndroid = /android/gi.test(i);
            t.isIOS = /iphone|ipad|ipod/gi.test(i);
            t.supportsTransition = bt();
            t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent);
            o = n(document);
            t.popupsCache = {}
        },
        open: function(e) {
            var c, w, a, b, v, k, y, d, p;
            if (l || (l = n(document.body)), e.isObj === !1) {
                for (t.items = e.items.toArray(), t.index = 0, w = e.items, c = 0; c < w.length; c++)
                    if (a = w[c], a.parsed && (a = a.el[0]), a === e.el[0]) {
                        t.index = c;
                        break
                    }
            } else t.items = n.isArray(e.items) ? e.items : [e.items], t.index = e.index || 0; if (t.isOpen) {
                t.updateItemHTML();
                return
            }
            for (t.types = [], s = "", t.ev = e.mainEl && e.mainEl.length ? e.mainEl.eq(0) : o, e.key ? (t.popupsCache[e.key] || (t.popupsCache[e.key] = {}), t.currTemplate = t.popupsCache[e.key]) : t.currTemplate = {}, t.st = n.extend(!0, {}, n.magnificPopup.defaults, e), t.fixedContentPos = t.st.fixedContentPos === "auto" ? !t.probablyMobile : t.st.fixedContentPos, t.st.modal && (t.st.closeOnContentClick = !1, t.st.closeOnBgClick = !1, t.st.showCloseBtn = !1, t.st.enableEscapeKey = !1), t.bgOverlay || (t.bgOverlay = h("bg").on("click" + r, function() {
                t.close()
            }), t.wrap = h("wrap").attr("tabindex", -1).on("click" + r, function(n) {
                t._checkIfClose(n.target) && t.close()
            }), t.container = h("container", t.wrap)), t.contentContainer = h("content"), t.st.preloader && (t.preloader = h("preloader", t.container, t.st.tLoading)), b = n.magnificPopup.modules, c = 0; c < b.length; c++) v = b[c], v = v.charAt(0).toUpperCase() + v.slice(1), t["init" + v].call(t);
            return i("BeforeOpen"), t.st.showCloseBtn && (t.st.closeBtnInside ? (u(ot, function(n, t, i, r) {
                i.close_replaceWith = it(r.type)
            }), s += " mfp-close-btn-in") : t.wrap.append(it())), t.st.alignTop && (s += " mfp-align-top"), t.fixedContentPos ? t.wrap.css({
                overflow: t.st.overflowY,
                overflowX: "hidden",
                overflowY: t.st.overflowY
            }) : t.wrap.css({
                top: f.scrollTop(),
                position: "absolute"
            }), (t.st.fixedBgPos === !1 || t.st.fixedBgPos === "auto" && !t.fixedContentPos) && t.bgOverlay.css({
                height: o.height(),
                position: "absolute"
            }), t.st.enableEscapeKey && o.on("keyup" + r, function(n) {
                n.keyCode === 27 && t.close()
            }), f.on("resize" + r, function() {
                t.updateSize()
            }), t.st.closeOnContentClick || (s += " mfp-auto-cursor"), s && t.wrap.addClass(s), k = t.wH = f.height(), y = {}, t.fixedContentPos && t._hasScrollBar(k) && (d = t._getScrollbarSize(), d && (y.marginRight = d)), t.fixedContentPos && (t.isIE7 ? n("body, html").css("overflow", "hidden") : y.overflow = "hidden"), p = t.st.mainClass, t.isIE7 && (p += " mfp-ie7"), p && t._addClassToMFP(p), t.updateItemHTML(), i("BuildControls"), n("html").css(y), t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || l), t._lastFocusedEl = document.activeElement, setTimeout(function() {
                t.content ? (t._addClassToMFP(g), t._setFocus()) : t.bgOverlay.addClass(g);
                o.on("focusin" + r, t._onFocusIn)
            }, 16), t.isOpen = !0, t.updateSize(k), i(st), e
        },
        close: function() {
            t.isOpen && (i(et), t.isOpen = !1, t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(ht), setTimeout(function() {
                t._close()
            }, t.st.removalDelay)) : t._close())
        },
        _close: function() {
            var u, f;
            i(a);
            u = ht + " " + g + " ";
            t.bgOverlay.detach();
            t.wrap.detach();
            t.container.empty();
            t.st.mainClass && (u += t.st.mainClass + " ");
            t._removeClassFromMFP(u);
            t.fixedContentPos && (f = {
                marginRight: ""
            }, t.isIE7 ? n("body, html").css("overflow", "") : f.overflow = "", n("html").css(f));
            o.off("keyup" + r + " focusin" + r);
            t.ev.off(r);
            t.wrap.attr("class", "mfp-wrap").removeAttr("style");
            t.bgOverlay.attr("class", "mfp-bg");
            t.container.attr("class", "mfp-container");
            t.st.showCloseBtn && (!t.st.closeBtnInside || t.currTemplate[t.currItem.type] === !0) && t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach();
            t._lastFocusedEl && n(t._lastFocusedEl).focus();
            t.currItem = null;
            t.content = null;
            t.currTemplate = null;
            t.prevHeight = 0;
            i(vt)
        },
        updateSize: function(n) {
            if (t.isIOS) {
                var u = document.documentElement.clientWidth / window.innerWidth,
                    r = window.innerHeight * u;
                t.wrap.css("height", r);
                t.wH = r
            } else t.wH = n || f.height();
            t.fixedContentPos || t.wrap.css("height", t.wH);
            i("Resize")
        },
        updateItemHTML: function() {
            var u = t.items[t.index],
                r, f, e;
            t.contentContainer.detach();
            t.content && t.content.detach();
            u.parsed || (u = t.parseEl(t.index));
            r = u.type;
            i("BeforeChange", [t.currItem ? t.currItem.type : "", r]);
            t.currItem = u;
            t.currTemplate[r] || (f = t.st[r] ? t.st[r].markup : !1, i("FirstMarkupParse", f), t.currTemplate[r] = f ? n(f) : !0);
            y && y !== u.type && t.container.removeClass("mfp-" + y + "-holder");
            e = t["get" + r.charAt(0).toUpperCase() + r.slice(1)](u, t.currTemplate[r]);
            t.appendContent(e, r);
            u.preloaded = !0;
            i(pt, u);
            y = u.type;
            t.container.prepend(t.contentContainer);
            i("AfterChange")
        },
        appendContent: function(n, r) {
            t.content = n;
            n ? t.st.showCloseBtn && t.st.closeBtnInside && t.currTemplate[r] === !0 ? t.content.find(".mfp-close").length || t.content.append(it()) : t.content = n : t.content = "";
            i(yt);
            t.container.addClass("mfp-" + r + "-holder");
            t.contentContainer.append(t.content)
        },
        parseEl: function(r) {
            var u = t.items[r],
                o, e, f;
            if (u.tagName ? u = {
                el: n(u)
            } : (o = u.type, u = {
                data: u,
                src: u.src
            }), u.el) {
                for (e = t.types, f = 0; f < e.length; f++)
                    if (u.el.hasClass("mfp-" + e[f])) {
                        o = e[f];
                        break
                    }
                u.src = u.el.attr("data-mfp-src");
                u.src || (u.src = u.el.attr("href"))
            }
            return u.type = o || t.st.type || "inline", u.index = r, u.parsed = !0, t.items[r] = u, i("ElementParse", u), t.items[r]
        },
        addGroup: function(n, i) {
            var u = function(r) {
                r.mfpEl = this;
                t._openClick(r, n, i)
            }, r;
            i || (i = {});
            r = "click.magnificPopup";
            i.mainEl = n;
            i.items ? (i.isObj = !0, n.off(r).on(r, u)) : (i.isObj = !1, i.delegate ? n.off(r).on(r, i.delegate, u) : (i.items = n, n.off(r).on(r, u)))
        },
        _openClick: function(i, r, u) {
            var o = u.midClick !== undefined ? u.midClick : n.magnificPopup.defaults.midClick,
                e;
            if (o || !(i.which === 2 || i.ctrlKey || i.metaKey)) {
                if (e = u.disableOn !== undefined ? u.disableOn : n.magnificPopup.defaults.disableOn, e)
                    if (n.isFunction(e)) {
                        if (!e.call(t)) return !0
                    } else if (f.width() < e) return !0;
                i.type && (i.preventDefault(), t.isOpen && i.stopPropagation());
                u.el = n(i.mfpEl);
                u.delegate && (u.items = r.find(u.delegate));
                t.open(u)
            }
        },
        updateStatus: function(n, r) {
            if (t.preloader) {
                tt !== n && t.container.removeClass("mfp-s-" + tt);
                r || n !== "loading" || (r = t.st.tLoading);
                var u = {
                    status: n,
                    text: r
                };
                i("UpdateStatus", u);
                n = u.status;
                r = u.text;
                t.preloader.html(r);
                t.preloader.find("a").on("click", function(n) {
                    n.stopImmediatePropagation()
                });
                t.container.addClass("mfp-s-" + n);
                tt = n
            }
        },
        _checkIfClose: function(i) {
            if (!n(i).hasClass(wt)) {
                var r = t.st.closeOnContentClick,
                    u = t.st.closeOnBgClick;
                if (r && u || !t.content || n(i).hasClass("mfp-close") || t.preloader && i === t.preloader[0]) return !0;
                if (i === t.content[0] || n.contains(t.content[0], i)) {
                    if (r) return !0
                } else if (u && n.contains(document, i)) return !0;
                return !1
            }
        },
        _addClassToMFP: function(n) {
            t.bgOverlay.addClass(n);
            t.wrap.addClass(n)
        },
        _removeClassFromMFP: function(n) {
            this.bgOverlay.removeClass(n);
            t.wrap.removeClass(n)
        },
        _hasScrollBar: function(n) {
            return (t.isIE7 ? o.height() : document.body.scrollHeight) > (n || f.height())
        },
        _setFocus: function() {
            (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
        },
        _onFocusIn: function(i) {
            if (i.target !== t.wrap[0] && !n.contains(t.wrap[0], i.target)) return t._setFocus(), !1
        },
        _parseMarkup: function(t, u, f) {
            var e;
            f.data && (u = n.extend(f.data, u));
            i(ot, [t, u, f]);
            n.each(u, function(n, i) {
                var u, f;
                if (i === undefined || i === !1) return !0;
                e = n.split("_");
                e.length > 1 ? (u = t.find(r + "-" + e[0]), u.length > 0 && (f = e[1], f === "replaceWith" ? u[0] !== i[0] && u.replaceWith(i) : f === "img" ? u.is("img") ? u.attr("src", i) : u.replaceWith('<img src="' + i + '" class="' + u.attr("class") + '" />') : u.attr(e[1], i))) : t.find(r + "-" + n).html(i)
            })
        },
        _getScrollbarSize: function() {
            if (t.scrollbarSize === undefined) {
                var n = document.createElement("div");
                n.id = "mfp-sbm";
                n.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;";
                document.body.appendChild(n);
                t.scrollbarSize = n.offsetWidth - n.clientWidth;
                document.body.removeChild(n)
            }
            return t.scrollbarSize
        }
    };
    n.magnificPopup = {
        instance: null,
        proto: v.prototype,
        modules: [],
        open: function(t, i) {
            return rt(), t = t ? n.extend(!0, {}, t) : {}, t.isObj = !0, t.index = i || 0, this.instance.open(t)
        },
        close: function() {
            return n.magnificPopup.instance && n.magnificPopup.instance.close()
        },
        registerModule: function(t, i) {
            i.options && (n.magnificPopup.defaults[t] = i.options);
            n.extend(this.proto, i.proto);
            this.modules.push(t)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;<\/button>',
            tClose: "Close (Esc)",
            tLoading: "Loading..."
        }
    };
    n.fn.magnificPopup = function(i) {
        var r, u, f, e;
        return rt(), r = n(this), typeof i == "string" ? i === "open" ? (f = nt ? r.data("magnificPopup") : r[0].magnificPopup, e = parseInt(arguments[1], 10) || 0, f.items ? u = f.items[e] : (u = r, f.delegate && (u = u.find(f.delegate)), u = u.eq(e)), t._openClick({
            mfpEl: u
        }, r, f)) : t.isOpen && t[i].apply(t, Array.prototype.slice.call(arguments, 1)) : (i = n.extend(!0, {}, i), nt ? r.data("magnificPopup", i) : r[0].magnificPopup = i, t.addGroup(r, i)), r
    };
    p = "inline";
    ut = function() {
        b && (w.after(b.addClass(c)).detach(), b = null)
    };
    n.magnificPopup.registerModule(p, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function() {
                t.types.push(p);
                u(a + "." + p, function() {
                    ut()
                })
            },
            getInline: function(i, r) {
                var f, u, e;
                return (ut(), i.src) ? (f = t.st.inline, u = n(i.src), u.length ? (e = u[0].parentNode, e && e.tagName && (w || (c = f.hiddenClass, w = h(c), c = "mfp-" + c), b = u.after(w).detach().removeClass(c)), t.updateStatus("ready")) : (t.updateStatus("error", f.tNotFound), u = n("<div>")), i.inlineElement = u, u) : (t.updateStatus("ready"), t._parseMarkup(r, {}, i), r)
            }
        }
    });
    lt = function(i) {
        if (i.data && i.data.title !== undefined) return i.data.title;
        var r = t.st.image.titleSrc;
        if (r) {
            if (n.isFunction(r)) return r.call(t, i);
            if (i.el) return i.el.attr(r) || ""
        }
        return ""
    };
    n.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"><\/div><figure><div class="mfp-img"><\/div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"><\/div><div class="mfp-counter"><\/div><\/div><\/figcaption><\/figure><\/div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image<\/a> could not be loaded.'
        },
        proto: {
            initImage: function() {
                var n = t.st.image,
                    i = ".image";
                t.types.push("image");
                u(st + i, function() {
                    t.currItem.type === "image" && n.cursor && l.addClass(n.cursor)
                });
                u(a + i, function() {
                    n.cursor && l.removeClass(n.cursor);
                    f.off("resize" + r)
                });
                u("Resize" + i, t.resizeImage);
                t.isLowIE && u("AfterChange", t.resizeImage)
            },
            resizeImage: function() {
                var n = t.currItem,
                    i;
                n && n.img && t.st.image.verticalFit && (i = 0, t.isLowIE && (i = parseInt(n.img.css("padding-top"), 10) + parseInt(n.img.css("padding-bottom"), 10)), n.img.css("max-height", t.wH - i))
            },
            _onImageHasSize: function(n) {
                n.img && (n.hasSize = !0, e && clearInterval(e), n.isCheckingImgSize = !1, i("ImageHasSize", n), n.imgHidden && (t.content && t.content.removeClass("mfp-loading"), n.imgHidden = !1))
            },
            findImageSize: function(n) {
                var i = 0,
                    u = n.img[0],
                    r = function(f) {
                        e && clearInterval(e);
                        e = setInterval(function() {
                            if (u.naturalWidth > 0) {
                                t._onImageHasSize(n);
                                return
                            }
                            i > 200 && clearInterval(e);
                            i++;
                            i === 3 ? r(10) : i === 40 ? r(50) : i === 100 && r(500)
                        }, f)
                    };
                r(1)
            },
            getImage: function(r, u) {
                var o = 0,
                    s = function() {
                        r && (r.img[0].complete ? (r.img.off(".mfploader"), r === t.currItem && (t._onImageHasSize(r), t.updateStatus("ready")), r.hasSize = !0, r.loaded = !0, i("ImageLoadComplete")) : (o++, o < 200 ? setTimeout(s, 100) : h()))
                    }, h = function() {
                        r && (r.img.off(".mfploader"), r === t.currItem && (t._onImageHasSize(r), t.updateStatus("error", c.tError.replace("%url%", r.src))), r.hasSize = !0, r.loaded = !0, r.loadError = !0)
                    }, c = t.st.image,
                    l = u.find(".mfp-img"),
                    f;
                return l.length && (f = document.createElement("img"), f.className = "mfp-img", r.img = n(f).on("load.mfploader", s).on("error.mfploader", h), f.src = r.src, l.is("img") && (r.img = r.img.clone()), f = r.img[0], f.naturalWidth > 0 ? r.hasSize = !0 : f.width || (r.hasSize = !1)), t._parseMarkup(u, {
                    title: lt(r),
                    img_replaceWith: r.img
                }, r), t.resizeImage(), r.hasSize ? (e && clearInterval(e), r.loadError ? (u.addClass("mfp-loading"), t.updateStatus("error", c.tError.replace("%url%", r.src))) : (u.removeClass("mfp-loading"), t.updateStatus("ready")), u) : (t.updateStatus("loading"), r.loading = !0, r.hasSize || (r.imgHidden = !0, u.addClass("mfp-loading"), t.findImageSize(r)), u)
            }
        }
    });
    at = function() {
        return ft === undefined && (ft = document.createElement("p").style.MozTransform !== undefined), ft
    };
    n.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function(n) {
                return n.is("img") ? n : n.find("img")
            }
        },
        proto: {
            initZoom: function() {
                var f = t.st.zoom,
                    o = ".zoom",
                    r;
                if (f.enabled && t.supportsTransition) {
                    var h = f.duration,
                        c = function(n) {
                            var r = n.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                u = "all " + f.duration / 1e3 + "s " + f.easing,
                                t = {
                                    position: "fixed",
                                    zIndex: 9999,
                                    left: 0,
                                    top: 0,
                                    "-webkit-backface-visibility": "hidden"
                                }, i = "transition";
                            return t["-webkit-" + i] = t["-moz-" + i] = t["-o-" + i] = t[i] = u, r.css(t), r
                        }, s = function() {
                            t.content.css("visibility", "visible")
                        }, e, n;
                    u("BuildControls" + o, function() {
                        if (t._allowZoom()) {
                            if (clearTimeout(e), t.content.css("visibility", "hidden"), r = t._getItemToZoom(), !r) {
                                s();
                                return
                            }
                            n = c(r);
                            n.css(t._getOffset());
                            t.wrap.append(n);
                            e = setTimeout(function() {
                                n.css(t._getOffset(!0));
                                e = setTimeout(function() {
                                    s();
                                    setTimeout(function() {
                                        n.remove();
                                        r = n = null;
                                        i("ZoomAnimationEnded")
                                    }, 16)
                                }, h)
                            }, 16)
                        }
                    });
                    u(et + o, function() {
                        if (t._allowZoom()) {
                            if (clearTimeout(e), t.st.removalDelay = h, !r) {
                                if (r = t._getItemToZoom(), !r) return;
                                n = c(r)
                            }
                            n.css(t._getOffset(!0));
                            t.wrap.append(n);
                            t.content.css("visibility", "hidden");
                            setTimeout(function() {
                                n.css(t._getOffset())
                            }, 16)
                        }
                    });
                    u(a + o, function() {
                        t._allowZoom() && (s(), n && n.remove(), r = null)
                    })
                }
            },
            _allowZoom: function() {
                return t.currItem.type === "image"
            },
            _getItemToZoom: function() {
                return t.currItem.hasSize ? t.currItem.img : !1
            },
            _getOffset: function(i) {
                var r, u;
                r = i ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem);
                var f = r.offset(),
                    e = parseInt(r.css("padding-top"), 10),
                    o = parseInt(r.css("padding-bottom"), 10);
                return f.top -= n(window).scrollTop() - e, u = {
                    width: r.width(),
                    height: (nt ? r.innerHeight() : r[0].offsetHeight) - o - e
                }, at() ? u["-moz-transform"] = u.transform = "translate(" + f.left + "px," + f.top + "px)" : (u.left = f.left, u.top = f.top), u
            }
        }
    });
    k = "retina";
    n.magnificPopup.registerModule(k, {
        options: {
            replaceSrc: function(n) {
                return n.src.replace(/.w+$/, function(n) {
                    return "@2x" + n
                })
            },
            ratio: 1
        },
        proto: {
            initRetina: function() {
                if (window.devicePixelRatio > 1) {
                    var i = t.st.retina,
                        n = i.ratio;
                    n = isNaN(n) ? n() : n;
                    n > 1 && (u("ImageHasSize." + k, function(t, i) {
                        i.img.css({
                            "max-width": i.img[0].naturalWidth / n,
                            width: "100%"
                        })
                    }), u("ElementParse." + k, function(t, r) {
                        r.src = i.replaceSrc(r, n)
                    }))
                }
            }
        }
    }),
    function() {
        var u = 1e3,
            i = "ontouchstart" in window,
            r = function() {
                f.off("touchmove" + t + " touchend" + t)
            }, t = ".mfpFastClick";
        n.fn.mfpFastClick = function(e) {
            return n(this).each(function() {
                var l = n(this),
                    s, a, v, y, h, o, c;
                if (i) l.on("touchstart" + t, function(n) {
                    h = !1;
                    c = 1;
                    o = n.originalEvent ? n.originalEvent.touches[0] : n.touches[0];
                    v = o.clientX;
                    y = o.clientY;
                    f.on("touchmove" + t, function(n) {
                        o = n.originalEvent ? n.originalEvent.touches : n.touches;
                        c = o.length;
                        o = o[0];
                        (Math.abs(o.clientX - v) > 10 || Math.abs(o.clientY - y) > 10) && (h = !0, r())
                    }).on("touchend" + t, function(n) {
                        (r(), h || c > 1) || (s = !0, n.preventDefault(), clearTimeout(a), a = setTimeout(function() {
                            s = !1
                        }, u), e())
                    })
                });
                l.on("click" + t, function() {
                    s || e()
                })
            })
        };
        n.fn.destroyMfpFastClick = function() {
            n(this).off("touchstart" + t + " click" + t);
            i && f.off("touchmove" + t + " touchend" + t)
        }
    }();
    rt()
}(window.jQuery || window.Zepto),
function(n, t, i, r) {
    var s = n(r),
        f = "waypoint.reached",
        e = function(n, i) {
            n.element.trigger(f, i);
            n.options.triggerOnce && n.element[t]("destroy")
        }, c = function(n, t) {
            if (!t) return -1;
            for (var i = t.waypoints.length - 1; i >= 0 && t.waypoints[i].element[0] !== n[0];) i -= 1;
            return i
        }, u = [],
        l = function(t) {
            n.extend(this, {
                element: n(t),
                oldScroll: 0,
                waypoints: [],
                didScroll: !1,
                didResize: !1,
                doScroll: n.proxy(function() {
                    var t = this.element.scrollTop(),
                        r = t > this.oldScroll,
                        f = this,
                        u = n.grep(this.waypoints, function(n) {
                            return r ? n.offset > f.oldScroll && n.offset <= t : n.offset <= f.oldScroll && n.offset > t
                        }),
                        o = u.length;
                    (this.oldScroll && t || n[i]("refresh"), this.oldScroll = t, o) && (r || u.reverse(), n.each(u, function(n, t) {
                            (t.options.continuous || n === o - 1) && e(t, [r ? "down" : "up"])
                        }))
                }, this)
            });
            n(t).bind("scroll.waypoints", n.proxy(function() {
                this.didScroll || (this.didScroll = !0, r.setTimeout(n.proxy(function() {
                    this.doScroll();
                    this.didScroll = !1
                }, this), n[i].settings.scrollThrottle))
            }, this)).bind("resize.waypoints", n.proxy(function() {
                this.didResize || (this.didResize = !0, r.setTimeout(n.proxy(function() {
                    n[i]("refresh");
                    this.didResize = !1
                }, this), n[i].settings.resizeThrottle))
            }, this));
            s.load(n.proxy(function() {
                this.doScroll()
            }, this))
        }, a = function(t) {
            var i = null;
            return n.each(u, function(n, r) {
                if (r.element[0] === t) return i = r, !1
            }), i
        }, o = {
            init: function(r, e) {
                return this.each(function() {
                    var o = n.fn[t].defaults.context,
                        s, h = n(this);
                    e && e.context && (o = e.context);
                    n.isWindow(o) || (o = h.closest(o)[0]);
                    s = a(o);
                    s || (s = new l(o), u.push(s));
                    var y = c(h, s),
                        p = y < 0 ? n.fn[t].defaults : s.waypoints[y].options,
                        v = n.extend({}, p, e);
                    v.offset = v.offset === "bottom-in-view" ? function() {
                        var t = n.isWindow(o) ? n[i]("viewportHeight") : n(o).height();
                        return t - n(this).outerHeight()
                    } : v.offset;
                    y < 0 ? s.waypoints.push({
                        element: h,
                        offset: null,
                        options: v
                    }) : s.waypoints[y].options = v;
                    r && h.bind(f, r);
                    e && e.handler && h.bind(f, e.handler)
                }), n[i]("refresh"), this
            },
            remove: function() {
                return this.each(function(t, i) {
                    var r = n(i);
                    n.each(u, function(n, t) {
                        var i = c(r, t);
                        i >= 0 && (t.waypoints.splice(i, 1), t.waypoints.length || (t.element.unbind("scroll.waypoints resize.waypoints"), u.splice(n, 1)))
                    })
                })
            },
            destroy: function() {
                return this.unbind(f)[t]("remove")
            }
        }, h = {
            refresh: function() {
                n.each(u, function(t, r) {
                    var u = n.isWindow(r.element[0]),
                        f = u ? 0 : r.element.offset().top,
                        o = u ? n[i]("viewportHeight") : r.element.height(),
                        s = u ? 0 : r.element.scrollTop();
                    n.each(r.waypoints, function(n, t) {
                        var u, i, h;
                        t && ((u = t.options.offset, i = t.offset, typeof t.options.offset == "function" ? u = t.options.offset.apply(t.element) : typeof t.options.offset == "string" && (h = parseFloat(t.options.offset), u = t.options.offset.indexOf("%") ? Math.ceil(o * (h / 100)) : h), t.offset = t.element.offset().top - f + s - u, t.options.onlyOnScroll) || (i !== null && r.oldScroll > i && r.oldScroll <= t.offset ? e(t, ["up"]) : i !== null && r.oldScroll < i && r.oldScroll >= t.offset ? e(t, ["down"]) : !i && r.element.scrollTop() > t.offset && e(t, ["down"])))
                    });
                    r.waypoints.sort(function(n, t) {
                        return n.offset - t.offset
                    })
                })
            },
            viewportHeight: function() {
                return r.innerHeight ? r.innerHeight : s.height()
            },
            aggregate: function() {
                var t = n();
                return n.each(u, function(i, r) {
                    n.each(r.waypoints, function(n, i) {
                        t = t.add(i.element)
                    })
                }), t
            }
        };
    n.fn[t] = function(i) {
        if (o[i]) return o[i].apply(this, Array.prototype.slice.call(arguments, 1));
        if (typeof i != "function" && i) {
            if (typeof i == "object") return o.init.apply(this, [null, i]);
            n.error("Method " + i + " does not exist on jQuery " + t)
        } else return o.init.apply(this, arguments)
    };
    n.fn[t].defaults = {
        continuous: !0,
        offset: 0,
        triggerOnce: !1,
        context: r
    };
    n[i] = function(n) {
        return h[n] ? h[n].apply(this) : h.aggregate()
    };
    n[i].settings = {
        resizeThrottle: 200,
        scrollThrottle: 100
    };
    s.load(function() {
        n[i]("refresh")
    })
}(jQuery, "waypoint", "waypoints", window);
$("#quiz").ready(function() {
    function v() {
        $("#getQProcessing").removeClass("mfp-hide");
        f = "Not Yet";
        e = $("#quizUser").val();
        o = $("#quizAge").val();
        s = $("#quizEmail").val();
        h = $("#quizMobile").val();
        c = $("#quizGender").is(":checked");
        var n = '{ "name":"' + e + '","age":"' + o + '","email":"' + s + '","mobile":"' + h + '","gender":' + c + "}";
        $.ajax({
            type: "POST",
            url: "/Api/Quizzify/generateQs",
            data: n,
            contentType: "application/json",
            success: function(n, t, i) {
                i.status == 200 && (f = i.getResponseHeader("encAns"), $("#getQProcessing").addClass("mfp-hide"), p(n), $("#quizOptionsDef").removeClass("mfp-hide"))
            }
        })
    }

    function p(n) {
        $("#quizContent > div > h2").html("<span color='dot'>Question - <\/span>");
        $("#incrementQuestions").html("<button id='newQ' title='Click to submit answer' class='submit'>Okay.<\/button>");
        i = n[0].split("|");
        r = n[1].split("|");
        i.pop();
        r.pop();
        i.length == r.length ? (t = 0, y(i, r, t)) : w()
    }

    function w() {
        $("#quizContent > div > h2").append("Something went wrong. Please try again later. <span class='fa fa-exclamation-triangle'><\/span>");
        $("#quizContent > div > h2 > span").append("<div id='retryIcon'><span class='fa fa-undo fa-4x'> Retry <\/span><\/div>");
        $("#retryIcon").click(function() {
            v()
        })
    }

    function y(n, t, i) {
        i == -1 || (i >= n.length ? ($("#quizContent").addClass("mfp-hide"), $("#processing").removeClass("mfp-hide"), b()) : ($("#quizContent > div > h2").html(n[i]), $("#options").html(t[i])))
    }

    function b() {
        var t = '{ "name":"' + e + '","age":"' + o + '","email":"' + s + '","mobile":"' + h + '","gender":' + c + "}",
            i = JSON.stringify(a);
        $.ajax({
            type: "POST",
            url: "/Api/Quizzify/produceResult",
            headers: {
                validateAgainst: f,
                userData: t
            },
            data: i,
            contentType: "application/json",
            success: function(t) {
                n = t;
                k()
            }
        })
    }

    function k() {
        var t = "Hey " + n[3] + ", here are the Results";
        $("#selectedUserAvatar").prependTo("#showcase");
        $("#QuizheadLine > h2").text(t);
        $.ajax({
            type: "GET",
            url: "/Api/Quizzify/getImage",
            data: {
                totalQ: n[0],
                correctQ: n[2]
            },
            processData: !0,
            contentType: "application/json",
            cache: !1,
            success: function(n) {
                $("#processing").addClass("mfp-hide");
                $("#scoreImage").attr("src", n)
            },
            error: function() {
                $("#scoreImage").attr("src", "~/images/quiz/error.jpg")
            }
        });
        $("#scoreCard").removeClass("mfp-hide");
        $("#sendResultsByEmail").removeClass("mfp-hide");
        $("#userEmail").val(n[5])
    }

    function d() {
        $("#warnEmailMsg").addClass("mfp-hide");
        validateEmail($("#userEmail").val()) ? ($("#userEmail").attr("disabled", !0), $("#emailResults").attr("disabled", !0), $("#marketingPreference").attr("disabled", !0), n[5] = $("#userEmail").val(), g()) : $("#warnEmailMsg").removeClass("mfp-hide")
    }

    function g() {
        $("#marketingPreference").addClass("mfp-hide");
        $("#emailSending").removeClass("mfp-hide");
        n[7] = $("#emailSendCheckbox").is(":checked");
        n[4] = null;
        $.ajax({
            type: "PUT",
            url: "/Api/Email/emailResults",
            data: JSON.stringify(n),
            contentType: "application/json",
            success: function() {
                $("#emailSending").addClass("mfp-hide");
                $("#emailSent").removeClass("mfp-hide")
            },
            error: function() {
                $("#emailSending").addClass("mfp-hide");
                $("#emailfailed").removeClass("mfp-hide")
            }
        })
    }

    function nt() {
        var n = $("input[type=radio]:checked").attr("id");
        a.push(n)
    }

    function u(n) {
        l = "#" + n + "Div";
        $(".avatars > div").not(l).css({
            display: "none"
        });
        $(".avatars").addClass("two columns");
        $(l).addClass("setProfilePic");
        $("#quizForm").removeClass("mfp-hide");
        $("#quizContent > div > h2").html("Introductions <span class='dot fa fa-comments'><\/span>")
    }

    function tt() {
        return $("#quizUser").val() != "" && $("#quizEmail").val() != "" ? validateName($("#quizUser").val()) && validateEmail($("#quizEmail").val()) && validatePhone($("#quizMobile").val()) ? !0 : !1 : !1
    }
    var t = -1,
        i, r = 0,
        a = [],
        f = "Not Yet",
        e, o, s, h, c, l, n;
    $(".avatars").click(function(n) {
        switch (n.target.id) {
            case "boy":
                u(n.target.id);
                break;
            case "girl":
                u(n.target.id);
                break;
            case "woman":
                u(n.target.id);
                break;
            case "man":
                u(n.target.id);
                break;
            case "youngMan":
                u(n.target.id)
        }
    });
    $("#quizAge").bind("input", function() {
        $("#ageText").text($("#quizAge").val())
    }).bind("change", function() {
        $("#ageText").text($("#quizAge").val())
    });
    $("i.fa-female").on("click", function() {
        $("#quizGender").prop("checked", !1)
    });
    $("i.fa-male").on("click", function() {
        $("#quizGender").prop("checked", !0)
    });
    $("#startQuiz").click(function(n) {
        if (n.preventDefault(), tt()) {
            $("#msgText").addClass("mfp-hide");
            $("quizForm").slideUp();
            var i = "  Quizzify " + $("#quizUser").val() + " ",
                t = $(".setProfilePic");
            t.css("height", "80px").css("width", "80px").css("border", "dotted").css("border-radius", "50%");
            $(".quizHeaderText").text(i);
            t.insertAfter(".quizHeaderText");
            $("#quizForm").addClass("mfp-hide");
            v()
        } else $("#msgText").removeClass("mfp-hide")
    });
    $("#quizContent").on("click", "#newQ", function(n) {
        n.preventDefault();
        nt();
        t = t + 1;
        y(i, r, t)
    });
    $("#emailResults").click(d)
});
! function(n) {
    "use strict";
    n.matchMedia = n.matchMedia || function(n) {
        var u, i = n.documentElement,
            f = i.firstElementChild || i.firstChild,
            r = n.createElement("body"),
            t = n.createElement("div");
        return t.id = "mq-test-1", t.style.cssText = "position:absolute;top:-100em", r.style.background = "none", r.appendChild(t),
        function(n) {
            return t.innerHTML = '&shy;<style media="' + n + '"> #mq-test-1 { width: 42px; }<\/style>', i.insertBefore(r, f), u = 42 === t.offsetWidth, i.removeChild(r), {
                matches: u,
                media: n
            }
        }
    }(n.document)
}(this),
function(n) {
    "use strict";

    function p() {
        y(!0)
    }
    var t = {};
    n.respond = t;
    t.update = function() {};
    var f = [],
        tt = function() {
            var t = !1;
            try {
                t = new n.XMLHttpRequest
            } catch (i) {
                t = new n.ActiveXObject("Microsoft.XMLHTTP")
            }
            return function() {
                return t
            }
        }(),
        w = function(n, t) {
            var i = tt();
            i && (i.open("GET", n, !0), i.onreadystatechange = function() {
                4 !== i.readyState || 200 !== i.status && 304 !== i.status || t(i.responseText)
            }, 4 !== i.readyState && i.send(null))
        };
    if (t.ajax = w, t.queue = f, t.regex = {
        media: /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi,
        keyframes: /@(?:\-(?:o|moz|webkit)\-)?keyframes[^\{]+\{(?:[^\{\}]*\{[^\}\{]*\})+[^\}]*\}/gi,
        urls: /(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,
        findStyles: /@media *([^\{]+)\{([\S\s]+?)$/,
        only: /(only\s+)?([a-zA-Z]+)\s?/,
        minw: /\([\s]*min\-width\s*:[\s]*([\s]*[0-9\.]+)(px|em)[\s]*\)/,
        maxw: /\([\s]*max\-width\s*:[\s]*([\s]*[0-9\.]+)(px|em)[\s]*\)/
    }, t.mediaQueriesSupported = n.matchMedia && null !== n.matchMedia("only all") && n.matchMedia("only all").matches, !t.mediaQueriesSupported) {
        var c, b, l, i = n.document,
            r = i.documentElement,
            e = [],
            o = [],
            u = [],
            a = {}, k = 30,
            s = i.getElementsByTagName("head")[0] || r,
            it = i.getElementsByTagName("base")[0],
            h = s.getElementsByTagName("link"),
            v = function() {
                var u, t = i.createElement("div"),
                    n = i.body,
                    o = r.style.fontSize,
                    e = n && n.style.fontSize,
                    f = !1;
                return t.style.cssText = "position:absolute;font-size:1em;width:1em", n || (n = f = i.createElement("body"), n.style.background = "none"), r.style.fontSize = "100%", n.style.fontSize = "100%", n.appendChild(t), f && r.insertBefore(n, r.firstChild), u = t.offsetWidth, f ? r.removeChild(n) : n.removeChild(t), r.style.fontSize = o, e && (n.style.fontSize = e), u = l = parseFloat(u)
            }, y = function(t) {
                var rt = "clientWidth",
                    ut = r[rt],
                    ft = "CSS1Compat" === i.compatMode && ut || i.body[rt] || ut,
                    p = {}, ct = h[h.length - 1],
                    et = (new Date).getTime(),
                    tt, g, nt, f, it;
                if (t && c && k > et - c) return n.clearTimeout(b), b = n.setTimeout(y, k), void 0;
                c = et;
                for (tt in e)
                    if (e.hasOwnProperty(tt)) {
                        var a = e[tt],
                            w = a.minw,
                            d = a.maxw,
                            ot = null === w,
                            st = null === d,
                            ht = "em";
                        w && (w = parseFloat(w) * (w.indexOf(ht) > -1 ? l || v() : 1));
                        d && (d = parseFloat(d) * (d.indexOf(ht) > -1 ? l || v() : 1));
                        a.hasquery && (ot && st || !(ot || ft >= w) || !(st || d >= ft)) || (p[a.media] || (p[a.media] = []), p[a.media].push(o[a.rules]))
                    }
                for (g in u) u.hasOwnProperty(g) && u[g] && u[g].parentNode === s && s.removeChild(u[g]);
                u.length = 0;
                for (nt in p) p.hasOwnProperty(nt) && (f = i.createElement("style"), it = p[nt].join("\n"), f.type = "text/css", f.media = nt, s.insertBefore(f, ct.nextSibling), f.styleSheet ? f.styleSheet.cssText = it : f.appendChild(i.createTextNode(it)), u.push(f))
            }, d = function(n, i, r) {
                var h = n.replace(t.regex.keyframes, "").match(t.regex.media),
                    c = h && h.length || 0,
                    l, a, f, v, u, p, w, s;
                for (i = i.substring(0, i.lastIndexOf("/")), l = function(n) {
                    return n.replace(t.regex.urls, "$1" + i + "$2$3")
                }, a = !c && r, i.length && (i += "/"), a && (c = 1), f = 0; c > f; f++)
                    for (a ? (v = r, o.push(l(n))) : (v = h[f].match(t.regex.findStyles) && RegExp.$1, o.push(RegExp.$2 && l(RegExp.$2))), p = v.split(","), w = p.length, s = 0; w > s; s++) u = p[s], e.push({
                        media: u.split("(")[0].match(t.regex.only) && RegExp.$2 || "all",
                        rules: o.length - 1,
                        hasquery: u.indexOf("(") > -1,
                        minw: u.match(t.regex.minw) && parseFloat(RegExp.$1) + (RegExp.$2 || ""),
                        maxw: u.match(t.regex.maxw) && parseFloat(RegExp.$1) + (RegExp.$2 || "")
                    });
                y()
            }, g = function() {
                if (f.length) {
                    var t = f.shift();
                    w(t.href, function(i) {
                        d(i, t.href, t.media);
                        a[t.href] = !0;
                        n.setTimeout(function() {
                            g()
                        }, 0)
                    })
                }
            }, nt = function() {
                for (var r = 0; r < h.length; r++) {
                    var i = h[r],
                        t = i.href,
                        u = i.media,
                        e = i.rel && "stylesheet" === i.rel.toLowerCase();
                    t && e && !a[t] && (i.styleSheet && i.styleSheet.rawCssText ? (d(i.styleSheet.rawCssText, t, u), a[t] = !0) : (!/^([a-zA-Z:]*\/\/)/.test(t) && !it || t.replace(RegExp.$1, "").split("/")[0] === n.location.host) && ("//" === t.substring(0, 2) && (t = n.location.protocol + t), f.push({
                        href: t,
                        media: u
                    })))
                }
                g()
            };
        nt();
        t.update = nt;
        t.getEmValue = v;
        n.addEventListener ? n.addEventListener("resize", p, !1) : n.attachEvent && n.attachEvent("onresize", p)
    }
}(this)
