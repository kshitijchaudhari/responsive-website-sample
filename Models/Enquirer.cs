﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace appOne.Models
{
    public class Enquirer
    {
        public class EnquirerEmail
        {
            [Required(ErrorMessage="So I should put 'Anonymous' when referring to you?")]
            public string uName { get; set; }
            [Required]
            [EmailAddress(ErrorMessage="How should I contact you? Gimme it.")]
            public string uEmail { get; set; }
            [Required(ErrorMessage = "Just saying Hello or something else.")]
            [StringLength(60, MinimumLength=5, ErrorMessage = "Let's keep it succinct and precise between 5 and 60 characters.")]
            public string[] uSubject { get; set; }
            [StringLength(2000, ErrorMessage="Way too many words; You need to meet me in person")]
            public string uMessage { get; set; }
        }
    }
}