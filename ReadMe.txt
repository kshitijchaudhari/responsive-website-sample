-------- Template for the website www.iamkc.info ----------
-----------------------------------------------------------

Please feel free to use this template for your personal use. Before hosting it online it would be nice to hear from you about the experience of using this template.

The website originally is developed as a C# MVC .NET web application with a Jquery, JavaScript and CSS3 elements.
The code presumes that at least version 9 of Internet Explorer, Firefox 34 and Chrome 32 is being used by the user.
Although not tested on Microsoft Edge, Safari and Opera; I am absolutely sure that it will work fine with these browser (provided they are updated to the latest version as of August 2015). 

Please feel free to contact me at contact@iamkc.info or kshitijchaudhari@outlook.com

-----------------------------------------------------------


* Image assets are missing (copyrights)
* Font files are not included since they are licensed.
* JS and CSS in clubbed to a single file for brevity (Unminified)
* MVC controller code is edited for passwords and keys
* MVC controller code is missing Quiz controller code; this is a seperate project for me.
* MVC scaffolding code for standard MVC .NET is not included



-----------------------------------------------------------


TO merge the code, just use a standard MVC .NET sample application. Use the controller and JS and CSS files with appropriate references. Include Jquery if not present by default; you can remove Modernizr (no references for it in my code). 

Essentially, this is just a framework for the website and you'll need to include the assets and required MVC structure to make it work.


















Thank you.





-----------------------------------
Credit to Sparrow and CV templates for CSS. 